Hello Matthew,

What works:  
Theoretically, all of it.  I found the assignment fairly confusing (especially when I got to the test plans).
In particular, one part that I'm not sure is done correctly is the random number element, 
because the test plan seemed to imply that you could control them, and this program cannot.
So any discrepency between the wounds in your test plan, and mine, are because of that.

(By all of it, I mean the server-client connection, the game, the logging, the JUnit tests, etc)

Networking solution:
Um, I based it losely off of Howards code, but I redid most of it by the end, as I wanted to make sure I understood the networking.
Instead of splitting the actual connection code into 2 classes (Client and Server), it's just all in the server.  If you run the server separately
it functions as a server, otherwise it works as the connection between a client and the server.  You can take a look at the class if you want to know more.

Test Suites:
Just the TestClass class.
To run it:
Run a server
Make a large number of rounds (I was using 400, so I know that works), and any number of players > 0 (I was using 1, so I know that works, but any number > 0 should)
Run the test class.
This should work, nothing was failing on my machine.

The jar for the client is in the Jar folder, and it is called clientapp.jar
The jar for the server is in the Jar folder too, and it is called serverapp.jar


A few more things to note:
The log doesn't empty after each game.  I was unsure if that was the expected behaviour, or clear each time, but I couldn't find anything on it, so I left it that way.
To run the game:
Start Server
In Server enter the #of rounds, then #of players
Then start ALL clients before continuing
Put in their name and IP for each of them
Follow instructions on screen

Where to find everything:
The Jars can be found in the Jar folder
The output log from a program is in the ouput folder
The code is in the src folder
The Testplan Logs are in the TestPlanLogs folder
The Readme is toplevel.

Final Words,
This represents quite a lot of work, I hope it works well, and does well.

Best,
Nick Martino 100934881