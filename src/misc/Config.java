package misc;

public class Config {
	public static final int serverPort = 24000;
	public static final String serverName = "localhost";
	public static int maxClients = 4;
	
	public static void setMaxPlayers(int newMax)
	{
		maxClients = newMax;
	}
}
