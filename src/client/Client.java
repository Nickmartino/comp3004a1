package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import misc.Strings;
import rulesEngine.GameEngine;
import server.Server;

public class Client {
	
	private GameEngine engine = new GameEngine();
	private Server server = new Server();
	
	public Client()
	{
		server.connect();
	}
	
	public void beginGame()
	{
		String userData = getInputData(Strings.nameReq);
		System.out.println(server.handle("AddPlayer "+ userData));
		String name = userData.split("\\s+")[0];
		boolean done = false;
		while(!done)
		{
			String selectCommand = engine.selectCommand(getInputData(Strings.selectCommand));
			String str = server.handle(selectCommand + " " + name);
			System.out.println(str);
			if (str.contains("GAME OVER"))
			{
				done = true;
			}
		}
		System.out.println("The game has ended folks!");
	}
	
	private String getInputData(String display) {
		System.out.println(display);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			return br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "Could not read data";
	}
}
