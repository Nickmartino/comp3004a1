package rulesEngine;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine {

	private ArrayList<String> moves = new ArrayList<String>();
	private ArrayList<String> target = new ArrayList<String>();
	private ArrayList<String> attack = new ArrayList<String>();
	private ArrayList<Integer> attackSpeed = new ArrayList<Integer>();
	private ArrayList<String> defense = new ArrayList<String>();
	private ArrayList<Integer> defenseSpeed = new ArrayList<Integer>();;
	
	private ArrayList<String> players = new ArrayList<String>();
	private ArrayList<String> ips = new ArrayList<String>();
	private ArrayList<String> validAttacks = new ArrayList<String>();
	private ArrayList<String> validDefense = new ArrayList<String>();
	private int maxASpeed = 3;
	private int maxDSpeed = 3;
	private int minASpeed = 1;
	private int minDSpeed = 1;
	private int targetCSpeed = 4;
	private int curRound;
	private int maxRounds;
	public boolean testEngine = false;
	
	private ArrayList<Integer> numWoundsSaved = new ArrayList<Integer>();
	
	public GameEngine() {
		validAttacks.add("Thrust");
		validAttacks.add("Swing");
		validAttacks.add("Smash");
		
		validDefense.add("Charge");
		validDefense.add("Dodge");
		validDefense.add("Duck");
		
	}

	public String selectCommand(String command) {
		String args[] = command.split("\\s+");
		target.add(args[0]);
		attack.add(args[1]);
		attackSpeed.add(Integer.parseInt(args[2]));
		defense.add(args[3]);
		defenseSpeed.add(Integer.parseInt(args[4]));
		return command;
	}
	
	public String selectCommand(String command, int playerID) {
		String args[] = command.split("\\s+");
		target.add(playerID, args[0]);
		attack.add(playerID, args[1]);
		attackSpeed.add(playerID, Integer.parseInt(args[2]));
		defense.add(playerID, args[3]);
		defenseSpeed.add(playerID, Integer.parseInt(args[4]));
		return command;
	}
	
	public void addInput(String data)
	{
		moves.add(data);
	}
	
	public String calculateOutput()
	{
		ArrayList<String> msg = new ArrayList<String>();
		ArrayList<Integer> numWounds = new ArrayList<Integer>();
		
		ArrayList<String> target = new ArrayList<String>();
		ArrayList<String> attack = new ArrayList<String>();
		ArrayList<Integer> attackSpeed = new ArrayList<Integer>();
		ArrayList<String> defense = new ArrayList<String>();
		ArrayList<Integer> defenseSpeed = new ArrayList<Integer>();
		ArrayList<String> user = new ArrayList<String>();
		
		for (int i = 0; i < moves.size(); i++)
		{
			String args[] = moves.get(i).split("\\s+");
			target.add(args[0]);
			attack.add(args[1]);
			attackSpeed.add(Integer.parseInt(args[2]));
			defense.add(args[3]);
			defenseSpeed.add(Integer.parseInt(args[4]));
			user.add(args[5]);
			numWounds.add(numWoundsSaved.get(i));
		}
		
		for (int i = 0; i < moves.size(); i++)
		{
			if (!players.contains(target.get(i)))
			{
				moves.clear();
				return "TARGET ERROR";
			}
			
			if (!validAttacks.contains(attack.get(i)))
			{
				moves.clear();
				return "ATTACK ERROR";
			}
			
			if (!validDefense.contains(defense.get(i)))
			{
				moves.clear();
				return "DEFENSE ERROR";
			}
			
			if (attackSpeed.get(i) > maxASpeed || attackSpeed.get(i) < minASpeed)
			{
				moves.clear();
				return "ASPEED ERROR";
			}
			
			if (defenseSpeed.get(i) > maxDSpeed || defenseSpeed.get(i) < minDSpeed)
			{
				moves.clear();
				return "DSPEED ERROR";
			}
			
			if (defenseSpeed.get(i) + attackSpeed.get(i) != targetCSpeed)
			{
				moves.clear();
				return "CSPEED ERROR";
			}
		}
		
		//This is here to remove the randomness from the test cases
		if (!testEngine)
		{
			for (int i = 0; i < moves.size(); i++)
			{
				Random random = new Random();
				int r = random.nextInt(6) + 1;
				
				String attacks = attack.get(i);
				
				//Now we roll the die
				if (attacks.equals("Thrust") && (r == 1 || r == 2))
				{
					attacks = "Thrust";
				}else if (attacks.equals("Thrust") && (r == 3 || r == 4))
				{
					attacks = "Smash";
				}else if (attacks.equals("Thrust") && (r == 5 || r == 6))
				{
					attacks = "Swing";
				}else if (attacks.equals("Swing") && (r == 1 || r == 2))
				{
					attacks = "Swing";
				}else if (attacks.equals("Swing") && (r == 3 || r == 4))
				{
					attacks = "Thrust";
				}else if (attacks.equals("Swing") && (r == 5 || r == 6))
				{
					attacks = "Smash";
				}else if (attacks.equals("Smash") && (r == 1 || r == 2))
				{
					attacks = "Smash";
				}else if (attacks.equals("Smash") && (r == 3 || r == 4))
				{
					attacks = "Swing";
				}else if (attacks.equals("Smash") && (r == 5 || r == 6))
				{
					attacks = "Thrust";
				}
				
				attack.set(i, attacks);
			}
		}
		
		for (int i = 0; i < moves.size(); i++)
		{
			int targetIndex = user.indexOf(target.get(i));
			
			boolean speedAllowsWounds = true;
			if (attackSpeed.get(i).intValue() < defenseSpeed.get(targetIndex).intValue())
			{
				numWounds.set(targetIndex, numWounds.get(targetIndex).intValue() + 1);
				speedAllowsWounds = false;
			}
			
			if (speedAllowsWounds)
			{
				if (attack.get(i).equals("Thrust") && defense.get(targetIndex).equals("Charge"))
				{
					numWounds.set(targetIndex, numWounds.get(targetIndex).intValue() + 1);
				}else if (attack.get(i).equals("Swing") && defense.get(targetIndex).equals("Dodge"))
				{
					numWounds.set(targetIndex, numWounds.get(targetIndex).intValue() + 1);
				}else if (attack.get(i).equals("Smash") && defense.get(targetIndex).equals("Duck"))
				{
					numWounds.set(targetIndex, numWounds.get(targetIndex).intValue() + 1);
				}
			}
		}
		
		for (int i = 0; i < numWounds.size(); i++)
		{
			msg.add(target.get(i) + " " + attack.get(i) + " " + attackSpeed.get(i) + " " + defense.get(i) + " " + defenseSpeed.get(i));
		}
		
		for (int i = 0; i < numWounds.size(); i++)
		{
			msg.add(user.get(i) + " has " + numWounds.get(i) + " wounds!");
		}
		
		String bigMsg = "";
		
		for (int i = 0; i < msg.size(); i++)
		{
			bigMsg = bigMsg + "\t" + msg.get(i);
		}
		
		saveWounds(numWounds);
		
		
		moves.clear();
		
		return bigMsg;
	}
	
	private void saveWounds(ArrayList<Integer> numWounds)
	{
		for (int i = 0; i < numWounds.size(); i++)
		{
			numWoundsSaved.set(i, numWounds.get(i));
		}
	}
	
	public void clearWounds()
	{
		for (int i = 0; i < numWoundsSaved.size(); i++)
		{
			numWoundsSaved.set(i, 0);
		}
	}
	
	/*public String calculateOutput(String data)
	{
		String outputString = "TestOutput";
		
		String[] splitData = data.split("\\s+");
		String target = splitData[0];
		String attack = splitData[1];
		int attackSpeed = Integer.parseInt(splitData[2]);
		String defense = splitData[3];
		int defenseSpeed = Integer.parseInt(splitData[4]);
		
		if (!players.contains(target))
		{
			return "TARGET ERROR";
		}
		
		if (!validAttacks.contains(attack))
		{
			return "ATTACK ERROR";
		}
		
		if (!validDefense.contains(defense))
		{
			return "DEFENSE ERROR";
		}
		
		if (attackSpeed > maxASpeed || attackSpeed < minASpeed)
		{
			return "ASPEED ERROR";
		}
		
		if (defenseSpeed > maxDSpeed || defenseSpeed < minDSpeed)
		{
			return "DSPEED ERROR";
		}
		
		if (defenseSpeed + attackSpeed != targetCSpeed)
		{
			return "CSPEED ERROR";
		}
		
		//This is for test cases
		if (!data.contains("~~&&~~")){
			Random random = new Random();
			int r = random.nextInt(6) + 1;
			System.out.println("Random number = " + r);
			
			//Now we roll the die
			if (attack.equals("Thrust") && (r == 1 || r == 2))
			{
				attack = "Thrust";
			}else if (attack.equals("Thrust") && (r == 3 || r == 4))
			{
				attack = "Smash";
			}else if (attack.equals("Thrust") && (r == 5 || r == 6))
			{
				attack = "Swing";
			}else if (attack.equals("Swing") && (r == 1 || r == 2))
			{
				attack = "Swing";
			}else if (attack.equals("Swing") && (r == 3 || r == 4))
			{
				attack = "Thrust";
			}else if (attack.equals("Swing") && (r == 5 || r == 6))
			{
				attack = "Smash";
			}else if (attack.equals("Smash") && (r == 1 || r == 2))
			{
				attack = "Smash";
			}else if (attack.equals("Smash") && (r == 3 || r == 4))
			{
				attack = "Swing";
			}else if (attack.equals("Smash") && (r == 5 || r == 6))
			{
				attack = "Thrust";
			}
		}
		
		if (outputString.equals("TestOutput"))
		{
			outputString = target + " " + attack + " " + attackSpeed + " " + defense + " " + defenseSpeed;
		}
		
		return outputString;
	}*/
	
	public void addPlayer(String name, String ip)
	{
		players.add(name);
		ips.add(ip);
		numWoundsSaved.add(0);
	}
	
	public void removePlayer(String name)
	{
		players.remove(name);
	}
	
	public ArrayList<String> getPlayers()
	{
		return players;
	}
	
	public boolean incrementRound()
	{
		curRound++;
		if (curRound >= maxRounds)
		{
			return true;
		}
		return false;
	}
	
	public void setMaxRounds(int newMaxRounds)
	{
		maxRounds = newMaxRounds;
	}
}
