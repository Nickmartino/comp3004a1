package server;

import java.net.*;
import java.io.*;

public class ServerThread extends Thread {
	private int ID = -1;
	public Socket socket = null;
	private BufferedReader streamIn = null;
	private BufferedWriter streamOut = null;

	private String clientAddress = null;
	private boolean done = false;
	
	private String IP = "";

	public ServerThread(Socket socket) {
		super();
		this.socket = socket;
		this.ID = socket.getPort();
		this.clientAddress = socket.getInetAddress().getHostAddress();
	}

	public int getID() {
		return this.ID;
	}

	public String getSocketAddress() {
		return clientAddress;
	}

	/**
	 * The server processes the messages and passes it to the client to send it
	 */
	public void send(String msg) {
		if (!done)
		{
			try {
				streamOut.write(msg);
				streamOut.newLine();
				streamOut.flush();
			} catch (IOException ioe) {
				System.out.println(ioe);
			}
		}
	}

	/**
	 * server thread that listens for incoming message from the client on the
	 * assigned port
	 */
	public void run() {
		while (!done) {
			try {
				/** Received a message and pass to the server to handle */
				String str = streamIn.readLine();
				if (str != null && str.equals("Close"))
				{
					Server.remove(this);
				}
				if (str != null)
				{
					Server.receiveRespond(str, this);
				}
			} catch (IOException ioe) {
				System.out.println(ioe);
				break;
			}
		}
	}

	public void open() throws IOException {
		streamIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		streamOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
	}

	public void close() {

		try {
			if (socket != null)
				socket.close();
			if (streamIn != null)
				streamIn.close();

			this.done = true;
			this.socket = null;
			this.streamIn = null;
		} catch (IOException e) {
		}
	}
	
	public String getIP()
	{
		return IP;
	}
	
	public void setIP(String newIP)
	{
		IP = newIP;
	}

}