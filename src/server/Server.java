package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.ArrayList;

import misc.Config;
import misc.Strings;
import rulesEngine.GameEngine;

public class Server{

	private static ArrayList<ServerThread> clients;
	private static ServerSocket serverSocket;
	private static Thread thread = null;
	private static GameEngine engine = new GameEngine();
	
	private static ArrayList<String> msg = new ArrayList<String>();
	private static boolean ready = false;
	private static int calls = 0;

	public static void main(String args[]) {
		try {
			
			engine.setMaxRounds(Integer.parseInt(getInputData(Strings.roundReq))+1);
			Config.setMaxPlayers(Integer.parseInt(getInputData(Strings.playerReq)));
			
			clients = new ArrayList<ServerThread>();

			serverSocket = new ServerSocket(Config.serverPort);
			serverSocket.setReuseAddress(true);
			startServer();
		} catch (IOException ioe) {
			System.out.println(ioe);
		}
	}

	private static void startServer() {
		if (thread == null) {
			thread = new Thread();
			thread.start();
		}

		while (thread != null && serverSocket != null) {
			try {
				addThread(serverSocket.accept());
			} catch (IOException e) {
			}
		}
	}

	private static void addThread(Socket socket) {
		if (clients.size() < Config.maxClients) {
			try {
				/** Create a separate server thread for each client */
				ServerThread serverThread = new ServerThread(socket);
				/** Open and start the thread */
				serverThread.open();
				serverThread.start();
				clients.add(serverThread);
			} catch (IOException e) {
				System.out.println(e);
			}
		}
	}

	public static synchronized void receiveRespond(String data, ServerThread sender)
	{
		String logInput = "";
		String logOutput = "";
		if (data != null)
		{
			logInput = data;
			calls++;
			//This is used for testing
			if (data.equals("cat"))
			{
				msg.add("CAPITOL CRIMES");
				ready = true;
			}else if (data.split("\\s+").length == 6)
			{
				engine.addInput(data);
				msg.add("");
			} else if (data.split("\\s+").length == 3 && data.contains("AddPlayer")){
				engine.addPlayer(data.split("\\s+")[1], data.split("\\s+")[2]);
				sender.setIP(data.split("\\s+")[2]);
				msg.add("Player Added");
			} else if (data.split("\\s+").length == 2 && data.contains("RemovePlayer")){
				engine.removePlayer(data.split("\\s+")[1]);
				msg.add("Player Removed");
			}else{
				System.out.println(data);
				msg.add("Data is improper");
			}
			log(logInput, "Received", sender);
		}
		
		if (calls == clients.size())
		{
			ready = true;
		}
		
		if (ready)
		{
			String fullMsg = "";
			for (int i = 0; i < msg.size(); i++)
			{
				fullMsg = fullMsg + "\t" + msg.get(i);
			}
			if (msg.contains(""))
			{
				fullMsg = fullMsg + engine.calculateOutput();
			}
			msg.clear();
			
			boolean gameDone = engine.incrementRound();
			
			if (gameDone)
			{
				fullMsg = fullMsg + "\t" + "GAME OVER";
			}
			
			for (int i = 0; i < clients.size(); i++)
			{
				clients.get(i).send(fullMsg);
			}
			logOutput = fullMsg;
			log(logOutput, "Sent", sender);
			ready = false;
			calls = 0;
		}
	}
	
	public static void log(String toLog, String sOrR, ServerThread sender)
	{
		try {
			File myFile =  new File(System.getProperty("user.dir") + File.separator + "\\output\\Log.txt");
			File parentDir = myFile.getParentFile();
			if(! parentDir.exists())
			{
				parentDir.mkdirs();
			}
			PrintWriter w = new PrintWriter(new FileWriter(myFile, true));
			String preLog = "";
			preLog = preLog + "\tlocalhost/127.0.0.1 \t" + sOrR;
			if (sOrR.equals("Received"))
			{
				preLog = preLog + " \tSent By: " + sender.getIP();
				preLog = preLog + " \tSent To: localhost/127.0.0.1";
			}else if (sOrR.equals("Sent"))
			{
				preLog = preLog + " \tSent By: localhost/127.0.0.1";
				preLog = preLog + " \tSent To: " + sender.getIP();
			}
			java.util.Date date= new java.util.Date();
			preLog = preLog + "\t TIMESTAMP : " + new Timestamp(date.getTime()) + "\t\t";
			String postLog = "\n";
			String fullLog = preLog + toLog + postLog;
			w.write(fullLog);
			w.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Try and shutdown the client cleanly */
	public static synchronized void remove(ServerThread toTerminate) {
		clients.remove(toTerminate);

		toTerminate.close();
		toTerminate = null;
	}
	
	private static String getInputData(String display) {
		System.out.println(display);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			return br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "Could not read data";
	}

	private Socket socket;

	public Server() {

	}

	public String handle(String data) {
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			bw.write(data);
			bw.newLine();
			bw.flush();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String str = br.readLine();
			return str;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "No response from the server";
	}

	public void connect() {
		try {
			socket = new Socket(Config.serverName, Config.serverPort);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Connected to server: " + socket.getInetAddress());
		System.out.println("Connected to portid: " + socket.getLocalPort());
	}

	public void disconnect() {
		try {
			OutputStream os = socket.getOutputStream();
			PrintWriter pw = new PrintWriter(os, true);
			pw.println("Close");
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
