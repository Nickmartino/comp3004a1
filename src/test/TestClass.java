package test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import rulesEngine.GameEngine;
import server.Server;

public class TestClass {

	static GameEngine engine = new GameEngine();
	static Server server = new Server();
	
	final static String correctInputStringSelect = "Nick Swing 2 Charge 2 Nick";
	
	@BeforeClass
	public static void init() {
		server.connect();
		server.handle("AddPlayer Nick 192.196.1.101");


		engine.addPlayer("Nick", "192.196.1.101");
		engine.addPlayer("Bob", "192.196.4.404");
		engine.testEngine = true;
	}
	
	@AfterClass
	public static void cleanup() {
		server.disconnect();
	}
	
	@After
	public void after()
	{
		engine.clearWounds();
	}
	
	@Test
	public void testSelect5Arguments()
	{
		engine.selectCommand("Cat Swing 1 Charge 3");
		Assert.assertTrue(true);
	}
	
	@Test(expected = RuntimeException.class)
	public void testSelect4OrLessArguments()
	{
		engine.selectCommand("Cat Swing 1 Charge");
	}
	
	@Test
	public void testSelectMoreThan5Arguments()
	{
		engine.selectCommand("Cat Swing 1 Charge 3 Dog");
		Assert.assertTrue(true);
	}
	
	@Test
	public void testCanConnectToServer()
	{
		Assert.assertTrue(true);
	}
	
	@Test
	public void testCanSendToServer()
	{
		server.handle("Test Cat");
		Assert.assertTrue(true);
	}
	
	@Test
	public void testCanServerRespond()
	{
		String str = server.handle("cat");
		
		
		if (str.equals("\tCAPITOL CRIMES"))
		{
			Assert.assertTrue(true);
		}else{
			System.out.println("Err, how did we get : " + str);
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testCanServerRespondMoreThanOnce()
	{
		boolean respondedCorrectly = false;
		
		String str = server.handle("cat");
		if (str.equals("\tCAPITOL CRIMES"))
		{
			respondedCorrectly = true;
		}else{
			respondedCorrectly = false;
		}
		
		if (respondedCorrectly == true)
		{
			str = server.handle("cat");
			if (str.equals("\tCAPITOL CRIMES"))
			{
				respondedCorrectly = true;
			}else{
				respondedCorrectly = false;
			}
		}else{
			respondedCorrectly = false;
		}
		Assert.assertTrue(respondedCorrectly);
	}
	
	@Test
	public void testTargetValid()
	{
		String str = server.handle(correctInputStringSelect);

		if (!str.equals("\tTARGET ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testTargetInvalid()
	{
		String sendStr = "Banjo-Kazooe Swing 2 Charge 2 Nick";
		String str = server.handle(sendStr);

		if (str.equals("\tTARGET ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testAttackValid()
	{
		String str = server.handle(correctInputStringSelect);

		if (!str.equals("\tATTACK ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testAttackInvalid()
	{
		String sendStr = "Nick Guitar 2 Charge 2 Nick";
		String str = server.handle(sendStr);
		
		if (str.equals("\tATTACK ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testDefenseValid()
	{
		String str = server.handle(correctInputStringSelect);

		if (!str.equals("\tDEFENSE ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testDefenseInvalid()
	{
		String sendStr = "Nick Swing 2 Cactus 2 Nick";
		String str = server.handle(sendStr);

		if (str.equals("\tDEFENSE ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testAttackSpeedValid()
	{
		String str = server.handle(correctInputStringSelect);

		if (!str.equals("\tASPEED ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testDefenseSpeedValid()
	{
		String str = server.handle(correctInputStringSelect);

		if (!str.equals("\tDSPEED ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testAttackPlusDefenseSpeedValid()
	{
		String str = server.handle(correctInputStringSelect);
		
		if (!str.equals("\tCSPEED ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testAttackSpeedInvalidHigh()
	{
		String sendStr = "Nick Swing 6 Charge 2 Nick";
		String str = server.handle(sendStr);
		
		if (str.equals("\tASPEED ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testAttackSpeedInvalidLow()
	{
		String sendStr = "Nick Swing 0 Charge 2 Nick";
		String str = server.handle(sendStr);

		if (str.equals("\tASPEED ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testDefenseSpeedInvalidHigh()
	{
		String sendStr = "Nick Swing 2 Charge 6 Nick";
		String str = server.handle(sendStr);
		
		if (str.equals("\tDSPEED ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testDefenseSpeedInvalidLow()
	{
		String sendStr = "Nick Swing 2 Charge 0 Nick";
		String str = server.handle(sendStr);

		if (str.equals("\tDSPEED ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testAttackPlusDefenseSpeedInvalidHigh()
	{
		String sendStr = "Nick Swing 3 Charge 3 Nick";
		String str = server.handle(sendStr);
		
		if (str.equals("\tCSPEED ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testAttackPlusDefenseSpeedInvalidLow()
	{
		String sendStr = "Nick Swing 1 Charge 1 Nick";
		String str = server.handle(sendStr);
		
		if (str.equals("\tCSPEED ERROR"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	//Succeed
	
	@Test
	public void testThrustVsCharge()
	{
		String str1 = "Bob Thrust 2 Charge 2";
		String str2 = "Nick Thrust 2 Duck 2";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 1 wounds!\tBob has 0 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testSwingVsDodge()
	{
		String str1 = "Bob Thrust 2 Dodge 2";
		String str2 = "Nick Swing 2 Duck 2";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 1 wounds!\tBob has 0 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testSmashVsDuck()
	{
		String str1 = "Bob Thrust 2 Duck 2";
		String str2 = "Nick Smash 2 Duck 2";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 1 wounds!\tBob has 0 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testASpeedLessDSpeed()
	{
		String str1 = "Bob Thrust 2 Duck 2";
		String str2 = "Nick Smash 1 Duck 3";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 1 wounds!\tBob has 1 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	//Fail
	
	@Test
	public void testThrustVsDodge()
	{
		String str1 = "Bob Thrust 2 Dodge 2";
		String str2 = "Nick Thrust 2 Duck 2";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 0 wounds!\tBob has 0 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testThrustVsDuck()
	{
		String str1 = "Bob Thrust 2 Duck 2";
		String str2 = "Nick Thrust 2 Duck 2";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 0 wounds!\tBob has 0 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testSwingVsCharge()
	{
		String str1 = "Bob Thrust 2 Charge 2";
		String str2 = "Nick Swing 2 Duck 2";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 0 wounds!\tBob has 0 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testSwingVsDuck()
	{
		String str1 = "Bob Thrust 2 Duck 2";
		String str2 = "Nick Swing 2 Duck 2";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 0 wounds!\tBob has 0 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testSmashVsCharge()
	{
		String str1 = "Bob Thrust 2 Charge 2";
		String str2 = "Nick Smash 2 Duck 2";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 0 wounds!\tBob has 0 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testSmashVsDodge()
	{
		String str1 = "Bob Thrust 2 Dodge 2";
		String str2 = "Nick Smash 2 Duck 2";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 0 wounds!\tBob has 0 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testASpeedGreaterDSpeed()
	{
		String str1 = "Bob Thrust 3 Charge 1";
		String str2 = "Nick Smash 3 Duck 1";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 0 wounds!\tBob has 0 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void testASpeedEqualDSpeed()
	{
		String str1 = "Bob Thrust 2 Charge 2";
		String str2 = "Nick Smash 2 Dodge 2";
		engine.addInput(str1 + " Nick");
		engine.addInput(str2 + " Bob");
		String result = engine.calculateOutput();
		
		if (result.equals("\t" + str1 + "\t" + str2 + "\tNick has 0 wounds!\tBob has 0 wounds!"))
		{
			Assert.assertTrue(true);
		}else{
			Assert.assertTrue(false);
		}
	}
	
	
	
}
